package gf

const (
	// VERSION  is the current GoFrame version.
	VERSION = "v2.2.1"
	// AUTHORS is the authors of GoFrame.
	AUTHORS = "john<john@goframe.org>"
)
